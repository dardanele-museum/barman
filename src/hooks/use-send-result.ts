import { useCallback } from 'react';

export const useSendResult = () => {
  const onComplete = useCallback(() => {
    console.log('SEND NOW!');
  }, []);

  return {
    onComplete,
  };
};
