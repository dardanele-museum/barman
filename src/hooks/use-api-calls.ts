import { useCallback } from 'react';
const ASSETS_FOLDER = 'assets';
const TRANSLATIONS_JSON = 'translations.json';
const API_URL = 'https://pttportal.newamsterdam.pl/api';
const LOGIN_ENDPOINT = '/apiaccount/login';
const ADD_POINTS_ENDPOINT = '/Portal/AddPoints';

export const useApiCall = ({
  resourcesPath = '',
  tag = '',
}: {
  resourcesPath?: string;
  tag?: string;
}) => {
  const getTranslations = useCallback(async () => {
    const result = await fetch(
      resourcesPath
        ? `${resourcesPath}/${TRANSLATIONS_JSON}`
        : `${ASSETS_FOLDER}/${TRANSLATIONS_JSON}`,
    );
    return await result.json();
  }, [resourcesPath]);

  const login = useCallback(async () => {
    const url = `${API_URL}${LOGIN_ENDPOINT}`;
    const loginData = {
      username: 'api@api.com',
      password: 'R23S2oW!AP1',
    };
    const result: any = await fetch(url, {
      method: 'post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(loginData),
    });

    return result ? result.token : '';
  }, []);

  const addPoints = useCallback(
    async (points: number) => {
      const url = `${API_URL}${ADD_POINTS_ENDPOINT}`;
      const data = {
        rfidTag: tag,
        appName: 'labyrinth',
        points,
      };

      try {
        const token = login();
        await fetch(url, {
          method: 'put',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
          body: JSON.stringify(data),
        });
      } catch (error) {
        console.log('Error while adding points');
      }
    },
    [tag, login],
  );

  return {
    getTranslations,
    addPoints,
  };
};
