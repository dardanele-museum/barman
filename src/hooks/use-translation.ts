import { useCallback, useMemo } from 'react';

export const useTranslation = (
  translations: any,
  paths: { name: string; path: string[] }[],
) => {
  const getNodeByKey = useCallback((nodes: any[], key: string) => {
    return nodes.find(n => n.key === key).subModules;
  }, []);

  const parseParagraph = useCallback((text: any) => {
    const regex = /<p>/g;
    const regex2 = /<\/p>/g;
    const regex3 = /<br>/g;
    if (text) {
      return Object.keys(text).reduce((acc, cValue) => {
        return {
          ...acc,
          [cValue]: text[cValue]
            ? text[cValue]
                .replace(regex, '')
                .replace(regex2, '\n')
                .replace(regex3, '')
            : '',
        };
      }, {});
    }
    return '';
  }, []);

  const moduleTranslations = useMemo(() => {
    if (translations) {
      const result = {} as any;
      paths.forEach( p => {
        let node = translations.screens;
        p.path.forEach(p => {
          node = getNodeByKey(node, p);
        });

        node = node.reduce((acc: any, cValue: any) => {
          return {
            ...acc,
            [cValue.key]: parseParagraph(cValue.text),
          };
        }, {});

        result[p.name] = node;
      })

      return result;
    }
  }, [translations, paths]);

  return {
    moduleTranslations,
  };
};
