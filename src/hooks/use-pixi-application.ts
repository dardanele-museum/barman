import { useEffect, useState } from 'react';
import { Application } from 'pixi.js';

export const usePixiApplication = ({
  width,
  height,
  parent,
}: {
  width: number;
  height: number;
  parent: any;
}) => {
  const [application] = useState<Application>(
    new Application({
      width,
      height,
      backgroundColor: 0x000000,
      transparent: true,
    }),
  );

  useEffect(() => {
    if (parent && parent.current) {
      parent.current.appendChild(application.view);
    }
    return () => {
      if (application) {
        application.stage.destroy({
          children: true,
          texture: true,
          baseTexture: true,
        });
        application.destroy(true);
      }
    };
  }, [parent, application]);

  return { application };
};
