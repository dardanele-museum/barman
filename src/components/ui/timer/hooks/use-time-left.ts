import { useCallback, useEffect, useRef, useState } from 'react';

export const useTimeLeft = ({
  seconds = 60,
  onComplete,
}: {
  seconds: number;
  onComplete?: () => void;
}) => {
  const secondsLeft = useRef(seconds);

  const formatTime = useCallback((seconds: number) => {
    return new Date(seconds * 1000).toISOString().substr(14, 5);
  }, []);

  const [timeLeft, setTimeLeft] = useState(formatTime(seconds));

  useEffect(() => {
    const interval = setInterval(() => {
      if (secondsLeft.current > 0) {
        secondsLeft.current -= 1;
        setTimeLeft(formatTime(secondsLeft.current));
      } else {
        clearInterval(interval);
        onComplete?.();
      }
    }, 1000);

    return () => clearInterval(interval);
  }, [seconds]);

  return {
    timeLeft,
  };
};
