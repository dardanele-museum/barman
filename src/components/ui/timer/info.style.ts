import styled from 'styled-components';

export const Wrapper = styled.div<{ position: 'top' | 'bottom' }>`
  position: absolute;
  justify-content: space-between;
  top: ${props => (props.position === 'top' ? 30 : 1850)}px;
  transform: rotate(${props => (props.position === 'top' ? 180 : 0)}deg);
  width: 1080px;
  display: flex;
`;

export const InfoWrapper = styled.div`
  display: flex;
  margin-left: 70px;
  margin-right: 70px;
  align-items: center;
`;

export const Description = styled.div`
  color: #cba675;
  font-size: 20px;
`;

export const Spacer = styled.div`
  width: 10px;
`;
