import React from 'react';
import { Wrapper, InfoWrapper, Spacer, Description } from './info.style';
import { GradientText } from '../../screens/screen.styles';
import { useAppContext } from '../../appdata/hooks/use-app-context';
import { useTranslation } from '../../../hooks/use-translation';

export function Info({
  position,
  timeLeft,
  points,
}: {
  position: 'top' | 'bottom';
  timeLeft: string;
  points: number;
}) {
  const {
    app: { language, translations },
  } = useAppContext();

  const { moduleTranslations } = useTranslation(translations, [
    {
      name: 'barman-info',
      path: ['barman-info'],
    },
  ]);

  if (!moduleTranslations) return null;

  const t = moduleTranslations['barman-info'];

  return (
    <Wrapper position={position}>
      <InfoWrapper>
        <Description>{t['time-left'][language]}</Description>
        <Spacer />
        <GradientText fontSize={30}>{timeLeft}</GradientText>
      </InfoWrapper>
      <InfoWrapper>
        <GradientText fontSize={30}>{points}</GradientText>
        <Spacer />
        <Description>{t['beers-collected'][language]}</Description>
      </InfoWrapper>
    </Wrapper>
  );
}
