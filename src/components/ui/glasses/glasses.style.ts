import styled from 'styled-components';

export const Main = styled.div`
  position: absolute;
  top: 1480px;
  pointer-events: none;
  font-family: Manofa;
  width: 1080px;
  display: flex;
  justify-content: center;
  user-select: none;
`;
export const Glass = styled.div`
  position: relative;
`;
export const GlassText = styled.div`
  position: absolute;
  top: 110px;
  left: 60px;
  background-size: 100%;
  background-image: linear-gradient(90deg, #7f5122, #cba675, #966a39);
  transform: rotate(90deg);
  text-align: right;
  width: 190px;
  color: #31220d;
  font-size: 22px;
  -webkit-background-clip: text;
  -moz-background-clip: text;
  -webkit-text-fill-color: transparent;
  -moz-text-fill-color: transparent;
`;
