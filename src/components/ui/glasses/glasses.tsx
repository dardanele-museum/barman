import React from 'react';
import bottleEmpty1 from '../../../assets/images/bottles/bottle_empty_03.png';
import bottleFull1 from '../../../assets/images/bottles/bottle_full_03.png';
import bottleEmpty2 from '../../../assets/images/bottles/bottle_empty_04.png';
import bottleFull2 from '../../../assets/images/bottles/bottle_full_04.png';
import bottleEmpty3 from '../../../assets/images/bottles/bottle_empty_05.png';
import bottleFull3 from '../../../assets/images/bottles/bottle_full_05.png';
import { Main, Glass, GlassText } from './glasses.style';
import { useAppContext } from '../../appdata/hooks/use-app-context';
import { useTranslation } from '../../../hooks/use-translation';

export function Glasses() {
  const {
    game: { solutionsFound },
    app: { language, translations },
  } = useAppContext();

  const { moduleTranslations } = useTranslation(translations, [
    {
      name: 'barman-beers',
      path: ['barman-beers'],
    },
  ]);

  if (!moduleTranslations) return null;

  const t = moduleTranslations['barman-beers'];

  return (
    <Main>
      <Glass>
        <img src={solutionsFound.includes(0) ? bottleFull1 : bottleEmpty1} />
        <GlassText>{t['lager'][language]}</GlassText>
      </Glass>
      <Glass>
        <img src={solutionsFound.includes(1) ? bottleFull2 : bottleEmpty2} />
        <GlassText>{t['wheat'][language]}</GlassText>
      </Glass>
      <Glass>
        <img src={solutionsFound.includes(2) ? bottleFull3 : bottleEmpty3} />
        <GlassText>{t['porter'][language]}</GlassText>
      </Glass>
    </Main>
  );
}
