import React from 'react';
import barrel from '../../../assets/images/barrels/barrel_03.png';
import { Main, Barrel, BarrelText } from './barrels.style';
import { useAppContext } from '../../appdata/hooks/use-app-context';
import { useTranslation } from '../../../hooks/use-translation';

export function Barrels() {
  const {
    app: { language, translations },
  } = useAppContext();

  const { moduleTranslations } = useTranslation(translations, [
    {
      name: 'barman-beers',
      path: ['barman-beers'],
    },
  ]);

  if (!moduleTranslations) return null;

  const t = moduleTranslations['barman-beers'];

  return (
    <Main>
      <Barrel>
        <BarrelText>{t['lager'][language]}</BarrelText>
        <img src={barrel} />
      </Barrel>
      <Barrel>
        <BarrelText>{t['wheat'][language]}</BarrelText>
        <img src={barrel} />
      </Barrel>
      <Barrel>
        <BarrelText>{t['porter'][language]}</BarrelText>
        <img src={barrel} />
      </Barrel>
    </Main>
  );
}
