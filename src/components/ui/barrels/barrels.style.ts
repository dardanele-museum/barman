import styled from 'styled-components';

export const Main = styled.div`
  position: absolute;
  top: 240px;
  pointer-events: none;
  font-family: Manofa;
  width: 1080px;
  display: flex;
  justify-content: space-around;
  user-select: none;
`;

export const Barrel = styled.div`
  display: block;
`;
export const BarrelText = styled.div`
  background-size: 100%;
  background-image: linear-gradient(90deg, #7f5122, #cba675, #966a39);
  transform: rotate(90deg) translateX(-50%);
  text-align: right;
  color: #31220d;
  font-size: 22px;
  -webkit-background-clip: text;
  -moz-background-clip: text;
  -webkit-text-fill-color: transparent;
  -moz-text-fill-color: transparent;
  user-select: none;
`;
