import { useCallback, useEffect, useMemo, useState } from 'react';
import { useApiCall } from '../../../hooks/use-api-calls';

export type InitAppParameters = {
  language: string;
  tag: string;
  groupCount: number;
  path: string;
  resourcesPath: string;
};

export function useAppData(initParameters: InitAppParameters) {
  const { language, tag, groupCount, path, resourcesPath } = useMemo(
    () => initParameters,
    [initParameters],
  );

  const [translations, setTranslations] = useState(null);
  const { getTranslations } = useApiCall({ resourcesPath });

  useEffect(() => {
    getTranslations().then(data => setTranslations(data));
  }, [language]);

  const [points, setPoints] = useState<number>(0);
  const [solutionsFound, setSolutionsFound] = useState<number[]>([]);

  const addPoints = useCallback((value: number) => {
    setPoints(points => points + 10);
  }, []);

  const reset = useCallback(() => {
    setPoints(0);
    setSolutionsFound([]);
  }, []);

  const addSolution = useCallback(
    (id: number) => {
      setSolutionsFound(solutionsFound => [...solutionsFound, id]);
    },
    [solutionsFound],
  );

  return {
    game: {
      points,
      addPoints,
      reset,
      solutionsFound,
      addSolution,
    },
    app: {
      language,
      tag,
      groupCount,
      path,
      translations,
    },
  };
}
