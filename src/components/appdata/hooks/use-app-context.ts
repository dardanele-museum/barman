import React, { useContext } from 'react';

export const AppContext = React.createContext<{
  game: {
    points: number;
    addPoints: (value: number) => void;
    reset: () => void;
    addSolution: (id: number) => void;
    solutionsFound: number[];
  };
  app: {
    language: string;
    tag: string;
    groupCount: number;
    path: string;
    translations: any;
  };
}>({
  game: {
    points: 0,
    solutionsFound: [],
    addPoints: () => {},
    addSolution: () => {},
    reset: () => {},
  },
  app: {
    language: 'pl',
    tag: '',
    groupCount: 1,
    path: 'interactive',
    translations: null,
  },
});

export function useAppContext() {
  const { game, app } = useContext(AppContext);
  return {
    game,
    app,
  };
}
