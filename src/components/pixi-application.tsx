import React, { useRef } from 'react';
import { usePixiApplication } from '../hooks/use-pixi-application';
import { Game } from './game';

export function PixiApplication({
  width,
  height,
  onComplete,
}: {
  width: number;
  height: number;
  onComplete: () => void;
}) {
  const ref = useRef(null);
  const { application } = usePixiApplication({
    width,
    height,
    parent: ref,
  });

  return (
    <>
      {
        <div
          style={{ position: 'absolute', top: 0, left: 0, width, height }}
          ref={ref}></div>
      }
      <Game application={application} onComplete={onComplete} />
    </>
  );
}
