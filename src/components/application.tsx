import React from 'react';
import { app as appConfig } from '../config.json';
import { PixiApplication } from './pixi-application';
import { Screen } from './screens/screen';
import { ScreensaverScreen } from './screens/screensaver/screensaver-screen';
import { StartScreen } from './screens/startscreen/start-screen';
import { SummaryScreen } from './screens/summaryscreen/summary-screen';
import { useScreensaverTimer } from '../hooks/use-screensaver-timer';
import { useMachine } from '@xstate/react';
import {
  GAME_STATES,
  GameMachine,
  TRANSITIONS,
} from '../statemachine/state-machine';

export function Application() {
  const { width: appWidth, height: appHeight } = appConfig;
  const [state, send] = useMachine(GameMachine);
  useScreensaverTimer({
    state: state.value as any,
    goToScreensaver: () => send(TRANSITIONS.SHOW_SCREENSAVER),
  });

  return (
    <>
      <Screen>
        <>
          {state.value === GAME_STATES.GAME && (
            <>
              <PixiApplication
                onComplete={() => send(TRANSITIONS.SHOW_SUMMARY_SCREEN)}
                width={appWidth}
                height={appHeight}
              />
            </>
          )}
          {state.value === GAME_STATES.INIT && (
            <>
              <StartScreen
                onClick={() => send(TRANSITIONS.START_GAME)}
                state={state.value}
              />
            </>
          )}
          {state.value === GAME_STATES.SUMMARY && (
            <>
              <SummaryScreen
                onClick={() => send(TRANSITIONS.SHOW_INIT_SCREEN)}
                state={state.value}
              />
            </>
          )}
          {state.value === GAME_STATES.SCREENSAVER && (
            <>
              <ScreensaverScreen
                onClick={() => send(TRANSITIONS.SHOW_INIT_SCREEN)}
                state={state.value}
              />
            </>
          )}
        </>
      </Screen>
    </>
  );
}
