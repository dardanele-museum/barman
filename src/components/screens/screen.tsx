import React from 'react';
import { Main, Background } from './screen.styles';
import { useScreenBackground } from './hooks/use-screen-background';
import { GAME_STATES } from '../../statemachine/state-machine';

export function Screen({
  onClick,
  children,
  showBackground = true,
  state,
}: {
  onClick?: () => void;
  children: React.ReactElement | null;
  showBackground?: boolean;
  state?: GAME_STATES;
}) {
  const { background } = useScreenBackground(state);

  return (
    <Main onClick={onClick ? onClick : undefined}>
      {showBackground && (
        <Background>
          <img src={background} />
        </Background>
      )}
      {children}
    </Main>
  );
}
