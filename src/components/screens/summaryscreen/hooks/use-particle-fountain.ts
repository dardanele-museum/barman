import { Application } from 'pixi.js';
import { useEffect, useRef } from 'react';
import { Fountain } from '../../../pixi/fountain/fountain';
import { app as appConfig } from "../../../../config.json";

export const useParticleFountain = (application: Application | null) => {
  const fountain = useRef<null | Fountain>();
  const { width: appWidth, height: appHeight } = appConfig;

  useEffect(() => {
    if (application) {
      fountain.current = new Fountain();
      fountain.current.x = appWidth/2;
      fountain.current.y = appHeight/2 + 100;
      application.stage.addChild(fountain.current);
    }
    return () => {
      if (fountain && fountain.current) {
        fountain.current.destroy?.();
      }
    };
  }, [application]);
};
