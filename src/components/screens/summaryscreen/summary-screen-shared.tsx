import React from 'react';
import sack from '../../../assets/images/sack.png';
import { AnimationWrapper, Sack } from './summary-screen.styles';
import { GradientText, Spacer } from '../screen.styles';
import { useAppContext } from '../../appdata/hooks/use-app-context';
import { useTranslation } from '../../../hooks/use-translation';

export function SummaryScreenShared({ points }: { points: number }) {
  const {
    app: { language, translations },
  } = useAppContext();

  const { moduleTranslations } = useTranslation(translations, [
    {
      name: 'barman-screen-summary',
      path: ['barman-screens', 'barman-screen-summary'],
    },
  ]);

  if (!moduleTranslations) return null;
  const t = moduleTranslations['barman-screen-summary'];

  return (
    <AnimationWrapper>
      <Sack url={sack}>
        <Spacer height={120} />
        <GradientText fontSize={30}>
          {t['sum'][language]}
        </GradientText>
        <Spacer height={10} />
        <GradientText fontSize={110}>{points}</GradientText>
      </Sack>
    </AnimationWrapper>
  );
}
