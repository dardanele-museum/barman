import React, { useEffect, useRef } from 'react';
import { Screen } from '../screen';
import { Wrapper } from '../screen.styles';
import { SummaryScreenHalf } from './summary-screen-half';
import { SummaryScreenShared } from './summary-screen-shared';
import { Overlay } from './summary-screen.styles';
import { useAppContext } from '../../appdata/hooks/use-app-context';
import { useApiCall } from '../../../hooks/use-api-calls';
import { usePixiApplication } from '../../../hooks/use-pixi-application';
import { useParticleFountain } from './hooks/use-particle-fountain';
import { app as appConfig } from '../../../config.json';
import { GAME_STATES } from '../../../statemachine/state-machine';

export function SummaryScreen({
  onClick,
  state,
}: {
  onClick: () => void;
  state?: GAME_STATES;
}) {
  const {
    app: { tag },
    game: { points },
  } = useAppContext();
  const { addPoints } = useApiCall({ tag });
  const ref = useRef(null);
  const { width: appWidth, height: appHeight } = appConfig;

  useEffect(() => {
    if (points !== 0) {
      addPoints(points);
    }
  }, []);

  const { application } = usePixiApplication({
    width: appWidth,
    height: appHeight,
    parent: ref,
  });

  useParticleFountain(application);

  return (
    <Screen onClick={onClick} showBackground={false} state={state}>
      <>
        <Overlay />
        <div
          style={{
            position: 'absolute',
            top: 0,
            left: 0,
            width: appWidth,
            height: appHeight,
            pointerEvents: 'none',
          }}
          ref={ref}></div>
        <Wrapper>
          <SummaryScreenHalf points={points} position="top" />
          <SummaryScreenShared points={points} />
          <SummaryScreenHalf points={points} position="bottom" />
        </Wrapper>
      </>
    </Screen>
  );
}
