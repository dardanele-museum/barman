import styled from 'styled-components';

export const Overlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 1080px;
  height: 1920px;
  background-color: #000000;
  opacity: 0.5;
`;

export const AnimationWrapper = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  height: 500px;
`;

export const Sack = styled.div<{ url: string }>`
  background: url(${props => props.url}) no-repeat;
  position: relative;
  width: 362px;
  height: 333px;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
`;
