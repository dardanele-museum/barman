import React from 'react';
import { HalfScreenWrapper, GradientText, Spacer } from '../screen.styles';
import { useAppContext } from '../../appdata/hooks/use-app-context';
import { useTranslation } from '../../../hooks/use-translation';

export function SummaryScreenHalf({
  position,
  points,
}: {
  position: 'top' | 'bottom';
  points: number;
}) {
  const {
    app: { language, translations },
  } = useAppContext();

  const { moduleTranslations } = useTranslation(translations, [
    {
      name: 'barman-screen-summary',
      path: ['barman-screens', 'barman-screen-summary'],
    },
  ]);

  if (!moduleTranslations) return null;
  const t = moduleTranslations['barman-screen-summary'];

  return (
    <HalfScreenWrapper position={position}>
      <GradientText>{t['title'][language]}</GradientText>
      <Spacer height={10} />
      <GradientText
        fontSize={
          40
        }>{`${t['summary'][language]} ${points} ${t['beers'][language]}`}</GradientText>
      <Spacer height={75} />
      <GradientText fontSize={40}>{t['prize'][language]}</GradientText>
      <Spacer height={10} />
      <GradientText
        fontSize={40}>{`${points} ${t['ducats'][language]}`}</GradientText>
    </HalfScreenWrapper>
  );
}
