import styled from 'styled-components';

export const Hand = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 130px;
  height: 130px;
  background-color: #5d3e08;
`;
