import React from 'react';
import { ScreensaverScreenHalf } from './screensaver-screen-half';
import { Wrapper } from '../screen.styles';
import { Screen } from '../screen';
import { GAME_STATES } from '../../../statemachine/state-machine';

export function ScreensaverScreen({
  onClick,
  state,
}: {
  onClick: () => void;
  state?: GAME_STATES;
}) {
  return (
    <Screen onClick={onClick} showBackground={false} state={state}>
      <Wrapper>
        <ScreensaverScreenHalf position="top" />
        <ScreensaverScreenHalf position="bottom" />
      </Wrapper>
    </Screen>
  );
}
