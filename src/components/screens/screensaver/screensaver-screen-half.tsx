import React from 'react';
import { Hand } from './screensaver-screen.style';
import { HalfScreenWrapper, Spacer, GradientText } from '../screen.styles';
import handImage from '../../../assets/images/hand.png';
import { useAppContext } from '../../appdata/hooks/use-app-context';
import { useTranslation } from '../../../hooks/use-translation';

export function ScreensaverScreenHalf({
  position,
}: {
  position: 'top' | 'bottom';
}) {
  const {
    app: { language, translations },
  } = useAppContext();

  const { moduleTranslations } = useTranslation(translations, [
    {
      name: 'barman-screen-screensaver',
      path: ['barman-screens', 'barman-screen-screensaver'],
    },
  ]);

  if (!moduleTranslations) return null;
  const t = moduleTranslations['barman-screen-screensaver'];

  return (
    <HalfScreenWrapper position={position}>
      <GradientText deg={45}>{t['info'][language]}</GradientText>
      <Spacer height={100} />
      <Hand>
        <img src={handImage} />
      </Hand>
    </HalfScreenWrapper>
  );
}
