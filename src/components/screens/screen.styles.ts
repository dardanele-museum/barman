import styled from 'styled-components';

export const Main = styled.div`
  font-family: Manofa;
  white-space: pre-line;
  vertical-align: bottom;
  user-select: none;
  position: absolute;
  display: flex;
`;

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  width: 1080px;
  height: 1920px;
`;

export const HalfScreenWrapper = styled.div<{ position: 'top' | 'bottom' }>`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: ${props =>
    props.position === 'top' ? 'flex-start' : 'flex-end'};
  transform: rotate(${props => (props.position === 'top' ? 180 : 0)}deg);
`;

export type Gradient = {
  deg?: number;
  fontSize?: number;
}

export const Text = styled.div`
  background-size: 100%;
  background-image: linear-gradient(45deg, #7f5122, #cba675, #966a39);
  text-align: center;
  color: #31220d;
  font-size: 70px;
  -webkit-background-clip: text;
  -moz-background-clip: text;
  -webkit-text-fill-color: transparent;
  -moz-text-fill-color: transparent;
  user-select: none;
`;

export const GradientText = styled.div<Gradient>`
  background-size: 100%;
  background-image: linear-gradient(${props => props.deg ? props.deg : 0}deg, #7f5122, #cba675, #966a39);
  text-align: center;
  color: #31220d;
  font-size: ${props => props.fontSize ? props.fontSize : 70}px;
  -webkit-background-clip: text;
  -moz-background-clip: text;
  -webkit-text-fill-color: transparent;
  -moz-text-fill-color: transparent;
  user-select: none;
`;

export const Background = styled.div`
  position: absolute;
`;

type Spacer = {
  width?: number;
  height?: number;
};

export const Spacer = styled.div<Spacer>`
  height: ${props => (props.height === undefined ? 0 : props.height)}px;
  width: ${props => (props.width === undefined ? 0 : props.width)}px;
`;
