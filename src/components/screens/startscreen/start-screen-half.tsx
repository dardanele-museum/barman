import React from 'react';
import {
  ScreenTitle,
  TextYellow,
  TextWhite,
  TextWhiteSpan,
  StartButton,
} from './start-screen.styles';
import { HalfScreenWrapper, Spacer, GradientText } from '../screen.styles';
import { game } from '../../../config.json';
import { useAppContext } from '../../appdata/hooks/use-app-context';
import { useTranslation } from '../../../hooks/use-translation';

export function StartScreenHalf({ position }: { position: 'top' | 'bottom' }) {
  const {
    app: { language, translations },
  } = useAppContext();

  const { moduleTranslations } = useTranslation(translations, [
    {
      name: 'barman-screen-start',
      path: ['barman-screens', 'barman-screen-start'],
    },
  ]);

  if (!moduleTranslations) return null;

  const t = moduleTranslations['barman-screen-start'];

  console.log(t['rules'][language])
    console.log(language);

  return (
    <HalfScreenWrapper position={position}>
      <GradientText>{t['game-name'][language]}</GradientText>
      <Spacer height={55} />
      <ScreenTitle>{t['game-name'][language]}</ScreenTitle>
      <Spacer height={40} />
      <TextYellow>{t['rules'][language]}</TextYellow>
      <Spacer height={55} />
      <TextYellow>
        {t['time-to-solve'][language]}
        <TextWhiteSpan>{`${game.time} ${t['minutes'][language]}`}</TextWhiteSpan>
      </TextYellow>
      <Spacer height={55} />
      <TextYellow>{t['call-to-action'][language]}</TextYellow>
      <TextWhite>{t['ready'][language]}</TextWhite>
      <Spacer height={85} />
      <StartButton>{t['start'][language]}</StartButton>
    </HalfScreenWrapper>
  );
}
