import styled from 'styled-components';

export const ScreenTitle = styled.div`
  font-size: 50px;
  color: #ffffff;
`;

export const TextYellow = styled.div`
  font-size: 22px;
  text-align: center;
  color: #bd8938;
`;

export const TextWhite = styled.div`
  font-size: 22px;
  text-align: center;
  color: #ffffff;
`;

export const TextWhiteSpan = styled.span`
  font-size: 22px;
  text-align: center;
  color: #ffffff;
`;

export const StartButton = styled.div`
  display: flex;
  width: 480px;
  height: 125px;
  background-color: #5d3e08;
  font-size: 70px;
  justify-content: center;
  align-items: center;
  color: #ffffff;
`;
