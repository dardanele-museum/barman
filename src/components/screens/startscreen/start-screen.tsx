import React from 'react';
import { Wrapper } from '../screen.styles';
import { Screen } from '../screen';
import { StartScreenHalf } from './start-screen-half';
import {GAME_STATES} from "../../../statemachine/state-machine";

export function StartScreen({ onClick, state }: { onClick: () => void, state?: GAME_STATES; }) {
  return (
    <Screen onClick={onClick} showBackground={false} state={state}>
      <Wrapper>
        <StartScreenHalf position="top" />
        <StartScreenHalf position="bottom" />
      </Wrapper>
    </Screen>
  );
}
