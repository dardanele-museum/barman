import { useState } from 'react';
import bg from '../../../assets/images/background.jpg';
import { GAME_STATES } from '../../../statemachine/state-machine';

export const useScreenBackground = (
  gameState: GAME_STATES = GAME_STATES.INIT,
) => {
  const [background] = useState<string>(bg);

  return {
    background,
  };
};
