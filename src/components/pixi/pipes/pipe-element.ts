import {
  Sprite,
  Texture,
  Graphics,
  BLEND_MODES,
  AnimatedSprite,
} from 'pixi.js';
import { TimelineLite } from 'gsap';

export class PipeElement extends Sprite {
  static CHANGE = 'change';
  id = 0;
  orientation = 0;
  background: Sprite = new Sprite();
  pipe: Sprite = new Sprite();
  highlight: Graphics = new Graphics();
  timeline?: TimelineLite;
  locked = false;
  animation: AnimatedSprite | null = null;
  animationFlip: AnimatedSprite | null = null;

  constructor({
    id,
    texture,
    bgTexture,
    spritesheet,
    spritesheetFlip,
  }: {
    id: number;
    texture: Texture;
    bgTexture: Texture;
    spritesheet: any;
    spritesheetFlip: any;
  }) {
    super();
    this.id = id;
    this.prepareTimeline();
    this.createBG(bgTexture);
    this.createPipe(texture);
    this.animation = this.createPipeAnimation(spritesheet);
    this.animationFlip = this.createPipeAnimation(spritesheetFlip);
    this.createHighlight(bgTexture.width, bgTexture.height);

    this.interactive = true;
    this.addListeners();
  }

  prepareTimeline() {
    this.timeline = new TimelineLite({
      onComplete: () => {
        this.lock(false);
      },
    });
    this.timeline.to(this.highlight, 0.5, { alpha: 1 });
    this.timeline.to(this.highlight, 0.5, { alpha: 0, delay: 1 });
    this.timeline.pause();
  }

  createBG(texture: Texture) {
    this.background.texture = texture;
    this.addChild(this.background);
  }

  createPipe(texture: Texture) {
    this.pipe.texture = texture;
    this.pipe.anchor.set(0.5);
    this.pipe.x += 1 + this.pipe.width / 2;
    this.pipe.y += 1 + this.pipe.height / 2;
    this.addChild(this.pipe);
  }

  createPipeAnimation(spritesheet: any) {
    const animation = new AnimatedSprite(spritesheet);
    animation.anchor.set(0.5);
    animation.x += animation.width / 2;
    animation.y += animation.height / 2;
    animation.animationSpeed = 0.1;
    animation.loop = false;
    animation.onComplete = () => {
      animation.alpha = 0;
      animation?.gotoAndStop(0);
    };
    animation.alpha = 0;
    this.addChild(animation);
    return animation;
  }

  createHighlight(width: number, height: number) {
    this.highlight.beginFill(0x00ff000, 0.3);
    this.highlight.blendMode = BLEND_MODES.SOFT_LIGHT;
    this.highlight.drawRoundedRect(0, 0, width - 2, height - 2, 10);
    this.highlight.endFill();
    this.highlight.visible = true;
    this.highlight.alpha = 0;
    this.addChild(this.highlight);
  }

  showHighlight(delay: number, dir: string) {
    if (this.timeline) {
      this.timeline.delay(delay);
      this.timeline.restart(true);
      if (this.animation && this.animationFlip) {
        if (this.id !== 1) {
          if (this.orientation === 0 || this.orientation === 1) {
            this.animation.alpha = 1;
          } else {
            this.animationFlip.alpha = 1;

            this.animationFlip.rotation =
              ((this.orientation + 1) * Math.PI) / 2;
          }
        }else {
          if( this.orientation === 2 ){
            this.animation.rotation = 0;
            this.animationFlip.rotation = 0;
          }
          if( this.orientation === 3 ){
            this.animation.rotation = Math.PI / 2;
            this.animationFlip.rotation = Math.PI / 2;
          }
          if( dir === 'right'){
            this.animationFlip.alpha = 1;
          }else{
            this.animation.alpha = 1;
          }

        }
      }
      setTimeout(() => {
        this.animation?.play();
        this.animationFlip?.play();
      }, delay * 1000);
    }
  }

  randomize() {
    this.orientation = Math.floor(Math.random() * 3);
    this.pipe.rotation = (this.orientation * Math.PI) / 2;

    if (this.animation && this.animationFlip) {
      this.animation.rotation = (this.orientation * Math.PI) / 2;
      this.animationFlip.rotation = (this.orientation * Math.PI) / 2;
    }
  }

  rotate() {
    if (this.locked) return;
    this.orientation = this.orientation + 1 > 3 ? 0 : this.orientation + 1;
    this.pipe.rotation = (this.orientation * Math.PI) / 2;

    if (this.animation && this.animationFlip) {
      this.animation.rotation = (this.orientation * Math.PI) / 2;
      this.animationFlip.rotation = (this.orientation * Math.PI) / 2;
    }

    this.emit(PipeElement.CHANGE);
  }

  addListeners() {
    this.addListener('pointerdown', this.rotate, this);
  }

  lock(value: boolean) {
    this.locked = value;
  }
}
