import { Container, Loader } from 'pixi.js';
import { data, solutions } from '../../../assets/data/barman.json';
import { PipeElement } from './pipe-element';

export class Pipes extends Container {
  loader: Loader = new Loader();
  pipes?: PipeElement[][];
  points = 0;
  solutionsFound: number[] = [];
  onSolutionFound?: (id: number) => void;
  onComplete?: () => void;

  constructor({ loader }: { loader: Loader }) {
    super();
    this.loader = loader;
    this.createPipes();
  }

  createPipes() {
    this.pipes = new Array(data.length).fill(0);
    for (let i = 0; i < data.length; i++) {
      this.pipes[i] = new Array(data[i].length).fill(0);
      for (let j = 0; j < data[i].length; j++) {
        const pipe = new PipeElement({
          id: data[i][j],
          texture: this.loader.resources[`pipe0${data[i][j]}`].texture,
          bgTexture: this.loader.resources['pipeBG'].texture,
          spritesheet: this.loader.resources['pipeAnimations'].spritesheet
            ?.animations[`${data[i][j]}pipe`],
          spritesheetFlip: this.loader.resources['pipeAnimations'].spritesheet
            ?.animations[`${data[i][j]}pipe_flip`],
        });
        pipe.x = pipe.width / 2 + j * 182;
        pipe.y = pipe.height / 2 + i * 182;
        pipe.randomize();
        pipe.addListener(PipeElement.CHANGE, this.checkSolutions, this);
        this.pipes[i][j] = pipe;
        this.addChild(pipe);
      }
    }

    this.x = (1080 - this.width) / 2;
    this.y = (1920 - this.height) / 2;
  }

  checkSolutions() {
    for (let j = 0; j < solutions.length; j++) {
      const result = this.checkSolution(solutions[j]);
      if (result && !this.solutionsFound.includes(j)) {
        this.solutionsFound.push(j);
        this.highlightSolution(solutions[j]);
        this.points += 10;
      }
    }
  }

  lockPipes(value: boolean) {
    if (this.pipes) {
      for (let i = 0; i < this.pipes.length; i++) {
        for (let j = 0; j < this.pipes[i].length; j++) {
          this.pipes[i][j].lock(value);
        }
      }
    }
  }

  highlightSolution(solution: any) {
    if (this.pipes) {
      this.lockPipes(true);

      for (let i = 0; i < solution.length; i++) {
        const x = solution[i].x;
        const y = solution[i].y;
        let dir = 'left';
        if (i > 0) {
          if (x > solution[i - 1].x) {
            dir = 'right';
          }
        }
        this.pipes[parseInt(y)][parseInt(x)].showHighlight(i * 0.4, dir);
      }
    }

    const newSolutionId = this.solutionsFound[this.solutionsFound.length - 1];

    const animationDuration = solution.length * 0.4 * 1000;

    setTimeout(() => {
      this.onSolutionFound?.(newSolutionId);
    }, animationDuration);

    setTimeout(() => {
      if (this.solutionsFound.length === solutions.length) {
        this.onComplete?.();
      } else {
        this.lockPipes(false);
      }
    }, animationDuration + 1000);
  }

  checkSolution(solution: any) {
    if (this.pipes) {
      for (let i = 0; i < solution.length; i++) {
        const x = solution[i].x;
        const y = solution[i].y;
        const values = solution[i].values;
        const orientation = this.pipes[parseInt(y)][parseInt(x)].orientation;
        if (!(values as any).includes(orientation)) {
          return false;
        }
      }
    } else {
      return false;
    }

    return true;
  }
}
