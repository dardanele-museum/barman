import { Loader } from 'pixi.js';
import { useEffect, useRef, useState } from 'react';
import pipe1 from '../../../assets/images/pipes/pipe_01.png';
import pipe2 from '../../../assets/images/pipes/pipe_02.png';
import pipe3 from '../../../assets/images/pipes/pipe_03.png';
import pipe4 from '../../../assets/images/pipes/pipe_04.png';
import pipeBG from '../../../assets/images/pipes/pipe_bg.png';

export const usePreloadAssets = () => {
  const [preloaded, setPreloaded] = useState(false);
  const loader = useRef<Loader>(new Loader());

  useEffect(() => {
    loader.current.add('pipe01', pipe1);
    loader.current.add('pipe02', pipe2);
    loader.current.add('pipe03', pipe3);
    loader.current.add('pipe04', pipe4);
    loader.current.add('pipeBG', pipeBG);
    loader.current.add(
      'pipeAnimations',
      'assets/images/spritesheets/pipes_final.json',
    );
    loader.current.onComplete.add(() => {
      setPreloaded(true);
    });
    loader.current.load();
  }, []);

  return {
    loader: loader.current,
    preloaded,
  };
};
