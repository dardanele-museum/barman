import { Application } from 'pixi.js';
import { useEffect, useState } from 'react';
import { Pipes } from '../pipes/pipes';
import {usePreloadAssets} from "./use-preload-assets";

export const useInitPixiObjects = ({
  application,
}: {
  application: Application | null;
}) => {
  const { preloaded, loader } = usePreloadAssets();
  const [pipes, setPipes] = useState<any>();

  useEffect(() => {
    if (application && preloaded) {
      const pipes = new Pipes({ loader });
      setPipes(pipes);
      application.stage.addChild(pipes);
    }
  }, [preloaded, application]);

  return {
    pipes
  };
};
