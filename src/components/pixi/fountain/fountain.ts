import * as particles from 'pixi-particles';
import * as PIXI from 'pixi.js';
import config from '../../../assets/particles/fountain/emitter.json';
import coin1 from '../../../assets/particles/fountain/coin/coin-1.png';
import coin2 from '../../../assets/particles/fountain/coin/coin-2.png';
import coin3 from '../../../assets/particles/fountain/coin/coin-3.png';
import coin4 from '../../../assets/particles/fountain/coin/coin-4.png';
import coin5 from '../../../assets/particles/fountain/coin/coin-5.png';
import coin6 from '../../../assets/particles/fountain/coin/coin-6.png';
import { AnimatedParticle } from 'pixi-particles';

export class Fountain extends PIXI.Container {
  myEmitter: particles.Emitter;
  elapsed: number;
  now: number;

  constructor() {
    super();

    this.elapsed = this.now = Date.now();
    this.myEmitter = new particles.Emitter(
      this,
      {
        framerate: '12',
        loop: true,
        textures: [
          PIXI.Texture.from(coin1),
          PIXI.Texture.from(coin2),
          PIXI.Texture.from(coin3),
          PIXI.Texture.from(coin4),
          PIXI.Texture.from(coin5),
          PIXI.Texture.from(coin6),
        ],
      },
      config,
    );

    this.myEmitter.particleConstructor = AnimatedParticle;
    this.myEmitter.emit = true;
    this.update.bind(this);
    this.update();
  }

  update() {
    requestAnimationFrame(this.update.bind(this));

    this.now = Date.now();
    this.myEmitter.update((this.now - this.elapsed) * 0.001);
    this.elapsed = this.now;
  }

  destroy() {
    this.myEmitter.destroy();
  }
}
