import React, { useEffect } from 'react';
import { useInitPixiObjects } from './pixi/hooks/use-init-pixi-objects';
import { Application } from 'pixi.js';
import { Info } from './ui/timer/info';
import { Barrels } from './ui/barrels/barrels';
import { Glasses } from './ui/glasses/glasses';
import { useTimeLeft } from './ui/timer/hooks/use-time-left';
import { game } from '../config.json';
import { useAppContext } from './appdata/hooks/use-app-context';

export function Game({
  application,
  onComplete,
}: {
  application: Application | null;
  onComplete: () => void;
}) {
  const { pipes } = useInitPixiObjects({ application });
  const { timeLeft } = useTimeLeft({
    seconds: game.time,
    onComplete: onComplete,
  });

  const { game: { points, addPoints, reset, addSolution } } = useAppContext();

  useEffect(() => {
    if (pipes) {
      reset();
      pipes.onSolutionFound = ( id: number ) => {
        addPoints(10);
        addSolution(id);
      };
      pipes.onComplete = onComplete;
    }
  }, [pipes]);

  return (
    <>
      <Info points={points} position="top" timeLeft={timeLeft} />
      <Info points={points} position="bottom" timeLeft={timeLeft} />
      <Barrels />
      <Glasses />
    </>
  );
}
