import { Machine } from 'xstate';

export enum GAME_STATES {
  INIT = 'init',
  SCREENSAVER = 'screensaver',
  GAME = 'game',
  SUMMARY = 'summary',
}

export enum TRANSITIONS {
  START_GAME = 'startGame',
  SHOW_SCREENSAVER = 'showScreensaver',
  SHOW_INIT_SCREEN = 'showInitScreen',
  SHOW_SUMMARY_SCREEN = 'showSummaryScreen',
}

export const GameMachine = Machine({
  id: 'gameStates',
  initial: GAME_STATES.INIT,
  states: {
    [GAME_STATES.INIT]: {
      on: {
        [TRANSITIONS.START_GAME]: GAME_STATES.GAME,
        [TRANSITIONS.SHOW_SCREENSAVER]: GAME_STATES.SCREENSAVER
      },
    },
    [GAME_STATES.GAME]: {
      on: {
        [TRANSITIONS.SHOW_SUMMARY_SCREEN]: GAME_STATES.SUMMARY,
      }
    },
    [GAME_STATES.SCREENSAVER]: {
      on: {
        [TRANSITIONS.SHOW_INIT_SCREEN]: GAME_STATES.INIT
      }
    },
    [GAME_STATES.SUMMARY]: {
      on: {
        [TRANSITIONS.SHOW_INIT_SCREEN]: GAME_STATES.INIT
      }
    },
  },
});
