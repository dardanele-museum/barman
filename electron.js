const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const path = require('path');
const isDev = require('electron-is-dev');

let mainWindow;

const argv = require('minimist')(process.argv.slice(2));
global.resourcesPath = process.resourcesPath;
global.argv = argv;

const createWindow = () => {
  const url = isDev
    ? 'http://localhost:3000'
    : `file://${path.join(__dirname, 'build/index.html')}`;

  mainWindow = new BrowserWindow({
    width: 900,
    height: 680,
    webPreferences: {
      nodeIntegration: true,
      enableRemoteModule: true
    },
  });
  mainWindow.loadURL(url);
  mainWindow.on('closed', () => (mainWindow = null));

  applySettings();
};

const applySettings = () => {
  mainWindow.removeMenu();

  if (!isDev) {
    mainWindow.fullScreen = true;
  }
};

app.whenReady().then(createWindow);
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});
app.on('activate', () => {
  if (mainWindow === null) {
    createWindow();
  }
});
